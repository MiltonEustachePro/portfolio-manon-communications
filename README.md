Hi there ! 

I've started this project during winter 2020, i wanted to discover nuxt.js in a practical way so i decided to made this portfolio, it is not finished because it was not needed anymore, but there's a few things that i learn during this project : 

- Nuxt.js and the way to deal with components, props, store, etc ... 
- Project structure
- I've gain some skills on css


I made another project for the company i use to work with in nuxt.js too. 
It was a project with drupal used as headless (only for backend) and nuxt.js as front end, it comes with deeper use of the nuxt.js store and axios.

Back to the current project, it's really an introduction for nuxt.js 

You just have run : npm run start in you terminal and you're good to go ! :) 

Cheers ! 


